const express=require("express")
const bodyParser=require("body-parser")
const cors=require("cors")
const Sequelize=require("sequelize")

const sequelize=new Sequelize('db_project','username1','1234',{
    dialect:'mysql'
})
let User=sequelize.define('user',
{
    email:{
        type:Sequelize.STRING,
        allowNull:false,
        primaryKey:true,
        validate:{
            len:[5,25],
            isEmail:true
        }
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[6,20]
        }
    },
    userName:{
        type:Sequelize.STRING,
        allowNull:false,
    }
    
});

let Project=sequelize.define('project', 
{
    id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    name:{
        type:Sequelize.TEXT,
        allowNull:false
    },
    description:{
        type:Sequelize.TEXT,
        allowNull:false,
    },
    link:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});
    
let Tester=sequelize.define('tester',{
    t_id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    }
    
});

let ProjectMember=sequelize.define('projectMember',{
     p_id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    }
});
    
let Bug=sequelize.define('bug',{
     id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    severity:{
        type:Sequelize.STRING,
        allowNull:false,
    },
    description:{
        type:Sequelize.TEXT,
        allowNull:false,
    },
    link:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});

let ResolvedBugs=sequelize.define('resolvedBug',{
     id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },solution:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});



Project.hasMany(ProjectMember, { onDelete: 'cascade' /*, foreignKey: 'id'*/ });
ProjectMember.belongsTo(Project/*, { foreignKey: 'id' }*/);
Project.hasMany(Tester, { onDelete: 'cascade'/*, foreignKey: 'id'*/ });
Tester.belongsTo(Project/*, { foreignKey: 'id' }*/);

User.hasMany(ProjectMember, { onDelete: 'cascade'/*, foreignKey: 'email' */});
ProjectMember.belongsTo(User/*, { foreignKey: 'email' }*/);
User.hasMany(Tester, { onDelete: 'cascade' /*, foreignKey: 'email'*/ });
Tester.belongsTo(User/*, { foreignKey: 'email' }*/);

Tester.hasMany(Bug, { onDelete: 'cascade' });
Bug.belongsTo(Tester);
ProjectMember.hasMany(ResolvedBugs, { onDelete: 'cascade' });
ResolvedBugs.belongsTo(ProjectMember);
Bug.hasOne(ResolvedBugs);
ResolvedBugs.belongsTo(Bug, { onDelete: 'cascade' });

const app=express()
app.use(cors())
app.use(bodyParser.json())

app.post('/sync',async(req,res,next)=>{
    try{
        await sequelize.sync({force:true})
        res.status(201).json({message:'created'})
    }catch(err)
    {
        next(err)
    }
})


app.post("/logIn", function(req, res,next) {
  try{
    const username1 = req.body.loginEmail;
    const password1 = req.body.loginPassword;
    if(username1 && password1){
        User.findAll({where:{email:username1,password:password1}}).then(function(User) {if (User != null && User.length==1) {
            var bemail=User.email;
            res.redirect("/user/"+bemail);
        }else res.redirect("/logIn");})
    }
  }catch(err)
    {
      next(err)
    }
})

app.post("/signUp",function(req,res,next){
    const bemail=req.body.signUpEmail;
    const pwd=req.body.signUpPassword;
    const pwdrp=req.body.repeatPassword;
    const user=req.body;
    const found=0;
    try{
        if(bemail&&pwd&&pwdrp&&pwd===pwdrp){
            User.findAll({where:{email:bemail}}).then(function(x){if(x!=null){found=1}})
            if(found==0){
               User.create(req.body.signUpEmail,req.body.signUpPassword,req.body.signUpEmail);
               //User.build(user.signUpEmail,user.repeatPassword,user.repeatPassword).save();
               res.redirect("/user/"+bemail);
            }
        }
    }catch(err){
        next(err)
    }
})

//add a user -> works
app.post('/user',async(req,res,next)=>{
    try{
       await User.create(req.body)
       res.status(201).send({message:'created'})
    }catch(err){
        next(err)
    }
})

//updating project when we know the id of the project 
app.put('/user/:bemail',async(req,res,next)=>{
    try{
        const result = await User.update({password:req.body},{ where: {email:req.params.bemail} })
    }catch(err){
        next(err)
    }
})

//get all users -> works
app.get('/users',async(req,res,next)=>{
    try{
        let users=await User.findAll()
        res.status(200).json(users)
    }catch(err){
        next(err)
    }
})

//get one user based by email -> works
app.get('/user/:bemail',async(req,res,next)=>{
    try{
        const  ID = req.params.bemail;
        let user=await User.findAll({where:{email:ID}});
        if(user){
        res.status(200).json(user)
        }
    }catch(err){
        next(err)
    }
})

//add a project -> adds project + adds row in projectmember of the creator of the project -> must add a for in order to add rows for the other project members
app.post('/user/:bemail/addproject',async(req,res,next)=>{
    try{
        //var bodyPM=(req.body,"userName");
        let pID=await Project.create(req.body).then( function ( x ) {return x.id; });
        //var proj = Project.build(req.body);
        //iterate through the members and add a row for each member in projectMember
        await ProjectMember.create({projectId:pID,userEmail:req.params.bemail});
       res.status(201).json({message:'created'})
    }catch(err){
        next(err)
    }
})

//for testing -> gets all rows in project member
app.get('/pm',async(req,res,next)=>{
    try{
        let projects=await ProjectMember.findAll();
        res.status(201).json(projects)
    }catch(err){
        next(err)
    }
})

//all projects no regard to user -> works
app.get('/projects',async(req,res,next)=>{
    try{
        let projects=await Project.findAll();
        res.status(201).json(projects)
    }catch(err){
        next(err)
    }
})

//all projects that the user's a member of 
app.get('/user/:bemail/projects',async(req,res,next)=>{
    try{
        let ps=await sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN projectMembers ON users.email=projectMembers.userEmail) INNER JOIN projects ON projects.id=projectMembers.projectId) WHERE users.email= :status', {replacements:{status:req.params.bemail}, type: sequelize.QueryTypes.SELECT});
        res.status(201).json(ps)
        //sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN projectMembers ON users.email=projectMembers.userEmail) INNER JOIN projects ON projects.id=projectMembers.projectId) WHERE users.email= :status', {replacements:{status:req.params.bemail}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})

//all the projects that the user is a tester at (done) + the bugs the user signaled for each project he's a tester (not done)
app.get('/user/:bemail/tester',async(req,res,next)=>{
    try{
        sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN projects ON projects.id=testers.projectId) WHERE users.email= :status', {replacements:{status:req.params.bemail}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})
//viewing the project from the project search bar; the user is not a tester or pm
//are on /user/:bemail/totalProjects -> click on a project --> /user/:bemail/undefined/project/:bid ; bid-> the id of the project
app.get('/user/:bemail/undefined/project/:bid',async(req,res,next)=>{
    try{
        sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM projects WHERE projects.id= :pID', {replacements:{pID:req.params.bid}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})


//viewing the project from the project member projects list
//are on /user/:bemail/projects -> click on a project  --> /user/:bemail/projectM/:bid/project ; bid-> teh id of the pm
app.get('/user/:bemail/projectM/:bid/project',async(req,res,next)=>{
    try{
        sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN projectMembers ON users.email=projectMembers.userEmail) INNER JOIN projects ON projects.id=projectMembers.projectId) WHERE users.email= :status AND projectMembers.p_id= :pID', {replacements:{status:req.params.bemail, pID:req.params.bid}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})

//viewing the project from the project member projects list
//are on /user/:bemail/projects -> click on a project  --> /user/:bemail/projectM/:bpid/:bid/project ; bid-> teh id of the project
app.get('/user/:bemail/projectM/:bpid/project/:bid',async(req,res,next)=>{
    try{
        sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN projectMembers ON users.email=projectMembers.userEmail) INNER JOIN projects ON projects.id=projectMembers.projectId) WHERE users.email= :status AND projectMembers.p_id= :pID AND projects.id= :projID', {replacements:{status:req.params.bemail, pID:req.params.bpid,projID:req.params.bid}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})

//updating project when we know the id of the project 
app.put('/project/:pid',async(req,res,next)=>{
    try{
        await Project.update({name:req.body,description:req.body,link:req.body},{ where: {id:req.params.pid} })
    }catch(err){
        next(err)
    }
})

//viewing the project from the tester projects
//are on /user/:bemail/tester -> click on a project --> /user/:bemail/tester/:bid/project ; bid-> the id of the project
app.get('/user/:bemail/tester/:btid/project/:bid',async(req,res,next)=>{
    try{
        sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN projects ON projects.id=testers.projectId) WHERE users.email= :status AND projects.id= :projID AND testers.t_id= :tID', {replacements:{status:req.params.bemail, projID:req.params.bid, tID:req.params.btid}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})

//viewing the project from the tester projects
//are on /user/:bemail/tester -> click on a project --> /user/:bemail/tester/:bid/project ; bid-> the id of the tester
app.get('/user/:bemail/tester/:bid/project',async(req,res,next)=>{
    try{
        sequelize.query('SELECT projects.id, projects.name, projects.description, projects.link FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN projects ON projects.id=testers.projectId) WHERE users.email= :status AND testers.t_id= :tID', {replacements:{status:req.params.bemail, tID:req.params.bid}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})

//all the bugs for a project
app.get('project/:bid/bugs',async(req,res,next)=>{
    try{
        sequelize.query('SELECT bugs.id, bugs.severity, bugs.description, bugs.link FROM ((projects INNER JOIN testers ON projects.id=testers.projectId) INNER JOIN bugs ON testers.t_id=bugs.testerTId) WHERE projects.id= :pid', {replacements:{pid:req.params.bid}, type: sequelize.QueryTypes.SELECT}).then(function(bugs) {res.status(201).json(bugs)})
    }catch(err){
        next(err)
    }
})

//the bugs the user has signaled for a project -> all the bugs for a specific tester
app.get('/user/:bemail/project/:bid/bugs',async(req,res,next)=>{
    try{
        sequelize.query('SELECT bugs.id, bugs.severity, bugs.description, bugs.link FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN projects ON projects.id=testers.projectId INNER JOIN bugs on testers.t_id=bugs.testerTId) WHERE users.email= :status AND projects.id= :pid', {replacements:{status:req.params.bemail, pid:req.params.bid}, type: sequelize.QueryTypes.SELECT}).then(function(projects) {res.status(201).json(projects)})
    }catch(err){
        next(err)
    }
})


//add a bug -> if we know only the user email and project id
app.post('/user/:bemail/project/pid/addBug',async(req,res,next)=>{
    try{
        //var bodyPM=(req.body,"userName");
        let tID=sequelize.query('SELECT testers.id FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN projects ON projects.id=testers.projectId) WHERE users.email= :status AND projects.id= :pid', {replacements:{status:req.params.bemail, pid:req.params.pid}, type: sequelize.QueryTypes.SELECT}).then(function(t) {return t.id});
        await Bug.create({severity:req.body,link:req.body,description:req.body,testerTId:tID});
        //var proj = Project.build(req.body);
        //iterate through the members and add a row for each member in projectMember
        //await ProjectMember.create({projectId:pID,userEmail:req.params.bemail});
       res.status(201).json({message:'created'})
    }catch(err){
        next(err)
    }
})

//add a bug if we know the tester id and project id
app.post('/tester/:bid/project/pid/addBug',async(req,res,next)=>{
    try{
        //var bodyPM=(req.body,"userName");
        await Bug.create({severity:req.body,link:req.body,description:req.body,testerTId:req.params.bid}).then( function ( x ) {return x.id; });
        //var proj = Project.build(req.body);
        //iterate through the members and add a row for each member in projectMember
        //await ProjectMember.create({projectId:pID,userEmail:req.params.bemail});
       res.status(201).json({message:'created'})
    }catch(err){
        next(err)
    }
})

//edit a bug if we know the bug id
app.put('/bug/:bid',async(req,res,next)=>{
    try{
        await Bug.update({severity:req.body,link:req.body,description:req.body},{ where: {id:req.params.bid} })
    }catch(err){
        next(err)
    }
})



app.use((err,req,res,next)=>{
    console.warn(err)
    res.status(500).json({message:'Error'})
})
app.listen(8080)