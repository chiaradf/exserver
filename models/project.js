let Project=sequelize.define('project', 
{
    id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    name:{
        type:Sequelize.TEXT,
        allowNull:false
    },
    description:{
        type:Sequelize.TEXT,
        allowNull:false,
    },
    link:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});
module.exports={Project}