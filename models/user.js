let User=sequelize.define('user',
{
    email:{
        type:Sequelize.STRING,
        allowNull:false,
        primaryKey:true,
        validate:{
            len:[5,25],
            isEmail:true
        }
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[6,20]
        }
    },
    userName:{
        type:Sequelize.STRING,
        allowNull:false,
    }
    
});
module.exports={User}